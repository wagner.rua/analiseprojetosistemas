# Generated by Django 2.1.1 on 2018-11-21 07:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TheCircleApp', '0008_auto_20181120_2252'),
    ]

    operations = [
        migrations.CreateModel(
            name='Artist',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('biography', models.CharField(max_length=1000)),
                ('country', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.AddField(
            model_name='artist',
            name='genre',
            field=models.ManyToManyField(to='TheCircleApp.Genre'),
        ),
    ]
