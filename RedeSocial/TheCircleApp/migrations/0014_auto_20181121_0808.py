# Generated by Django 2.1.1 on 2018-11-21 08:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TheCircleApp', '0013_auto_20181121_0747'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artist',
            name='biography',
            field=models.TextField(max_length=1000),
        ),
    ]
