from django.urls import path, include
from .views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth import views as auth_views


view_obj = Views()

urlpatterns = [
    path('', view_obj.login, name='login'),
    path('signup/', view_obj.signup, name='signup'),
    path('logout/', view_obj.logout, name='logout'),
    path('home/', view_obj.homePage, name='home'),
    path('home/edit/',view_obj.editProfile, name='edit'),
    path('person/<str:username>', view_obj.personPage),
    path('artist/<str:name>', view_obj.artistPage),
    path('film/<str:name>', view_obj.filmPage),
    path('blocked/', view_obj.heBlockedPage, name='heBlockedPage'),
]
