from django.contrib import admin
from TheCircleApp.models import *

admin.site.register(Profile)
admin.site.register(Genre)
admin.site.register(Artist)
admin.site.register(Film)
admin.site.register(FilmGenre)
admin.site.register(FilmArtist)
