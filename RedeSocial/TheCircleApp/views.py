from TheCircleApp.forms import SignUpForm, CustomAuthForm, EditProfileForm
from django.contrib.auth import authenticate, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect
from django.contrib.auth.forms import PasswordChangeForm
from TheCircleApp.models import Artist, Profile, User, Film, Search

class Views():
    def login(self, request):
        if request.method == 'POST':
            form = CustomAuthForm(data=request.POST)
            if form.is_valid():
                #log in the user
                user = form.get_user()
                user.profile.login(request)
                return redirect('home')
        else:
            form = CustomAuthForm()

        return render(request, 'login.html', {"is_login_or_signup_page" : True, 'form':form})

    def signup(self, request):
        if request.method == 'POST':
            form = SignUpForm(request.POST)
            if form.is_valid():
                user = form.save()
                user.profile.birth_date = form.get('birth_date')
                user.save()                                                                                                     
                return redirect('login')
            elif 'backbutton' in request.POST:                                                              
                return redirect('login')
        else:
            form = SignUpForm()

        return render(request, 'signup.html', {"is_login_or_signup_page" : True, "form" : form})

    def logout(self, request):
        if request.method == 'POST':
            user = request.user
            user.profile.logout(request)
            return redirect('login')

    @method_decorator(login_required)
    def homePage(self, request):
        show_result = False
        searched = []

        if request.POST.get('searchbutton') == 'Procura':
            name_to_search = request.POST.get('searchbox')
            show_result = True
            search = Search()
            searched = search.makeSearch(name_to_search)
            del search
            
        return render(request, './home_page.html', {"is_login_or_signup_page" : False, "show_result": show_result, "pesquisa": searched, "personPage": 0})

    @method_decorator(login_required)
    def editProfile(self, request):
        show_result = False
        searched = []
        search = Search()

        if request.POST.get('searchbutton') == 'Procura':
            name_to_search = request.POST.get('searchbox')
            show_result = True
            search = Search()
            searched = search.makeSearch(name_to_search)
            del search


        if request.method == 'POST':
            profile_form = EditProfileForm(request.POST, request.FILES, instance=request.user)
            password_form = PasswordChangeForm(data=request.POST, user=request.user)
            
            if profile_form.is_valid() and password_form.is_valid():
                user = profile_form.save()
                user = profile_form.updateForm(user)
                user.save()
                    
                password_form.save()
                update_session_auth_hash(request, password_form.user)
    
                return redirect('home')

            else:
                print(searched)
                return render(request,'edit_profile_page.html', {"is_login_or_signup_page" : False, "profile_form" : profile_form, "password_form" : password_form, "show_result": show_result, "pesquisa": searched})
        else:
            profile_form = EditProfileForm()
            password_form = PasswordChangeForm(user=request.user)

            return render(request,'edit_profile_page.html', {"is_login_or_signup_page" : False, "profile_form" : profile_form, "password_form" : password_form, "show_result": show_result, "pesquisa": searched})

    @method_decorator(login_required)
    def personPage(self, request, username):
        search = Search()
        user = search.getUser(username)
        search_user = user.profile

        logged_user = request.user.profile

        if logged_user == search_user:
            return redirect('home')

        show_result = False
        searched = []

        if request.POST.get('searchbutton') == 'Procura':
            name_to_search = request.POST.get('searchbox')
            show_result = True
            searched = search.makeSearch(name_to_search)


        if request.POST.get('btnAdd') == 'Conhecido':
            if search_user not in logged_user.getKnows():
                logged_user.addKnow(search_user)
            else:
                logged_user.removeKnow(search_user)

        btn_value = 'Conhecido'
        if search_user in logged_user.getKnows():
            btn_value = 'Desconhecido'

        block = "Bloquear"
        if search_user not in logged_user.getBlocks():
            if request.POST.get('save') == "save":
                logged_user.block(search_user)
                logged_user.save()
                block = "Desbloquear"
        else:
            if request.POST.get('btnBlock') == "unblock":
                logged_user.removeBlock(search_user)
                logged_user.save()
            else:
                block = "Desbloquear"

        if logged_user in search_user.getBlocks():
            return redirect('heBlockedPage')


        return render(request, './home_page.html', {"user" : user, "logged_user" : logged_user,
                    "block" : block,
                    "is_login_or_signup_page" : False, "show_result": show_result, "pesquisa": searched,
                    "btn_value": btn_value})

    @method_decorator(login_required)
    def artistPage(self, request, name):
        show_result = False
        searched = []
        search = Search()


        if request.POST.get('searchbutton') == 'Procura':
            name_to_search = request.POST.get('searchbox')
            show_result = True
            searched = search.makeSearch(name_to_search)

        
        artist = search.getArtist(name)
        user = request.user.profile

        if request.POST.get('btnAddFriend') == 'Curtir':
            if artist not in user.getLikeArtists():
                user.likeArtist(artist)	
            else:
                user.unlikeArtist(artist)

        btn_value = 'Curtir'
        if artist in user.getLikeArtists():
            btn_value = 'Descurtir'

        return render(request, './generic_page/artist_page.html', {"artist": artist, "is_login_or_signup_page" : False, "show_result": show_result, "pesquisa": searched, "btn_value": btn_value})

    @method_decorator(login_required)
    def filmPage(self, request, name):
        show_result = False
        searched = []
        search = Search()

        if request.POST.get('searchbutton') == 'Procura':
            name_to_search = request.POST.get('searchbox')
            show_result = True
            searched = search.makeSearch(name_to_search)

        film = search.getFilm(name)
        user = request.user.profile
        
        if request.POST.get('btnAddFriend') == 'Curtir':
            if film not in user.getLikeFilms():
                user.likeFilm(film)	
            else:
                user.unlikeFilm(film)

        btn_value = 'Curtir'
        if film in user.getLikeFilms():
            btn_value = 'Descurtir'

        return render(request, './generic_page/filme_page.html', {"film": film, "is_login_or_signup_page" : False, "show_result": show_result, "pesquisa": searched, "btn_value": btn_value})


    @method_decorator(login_required)
    def heBlockedPage(self, request):
        show_result = False
        searched = []

        if request.POST.get('searchbutton') == 'Procura':
            name_to_search = request.POST.get('searchbox')
            show_result = True
            search = Search()
            searched = search.makeSearch(name_to_search)
            del search

        return render(request, 'block_page.html', {"blocked": True, "is_login_or_signup_page" : False,
                                                    "show_result" : show_result, 'pesquisa' : searched})
