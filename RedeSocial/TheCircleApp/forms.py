from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.forms.widgets import PasswordInput, TextInput

CHOICES = (
    (1, ("1º")),
    (2, ("2º")),
    (3, ("3º")),
    (4, ("4º")),
    (5, ("5º")),
    (6, ("6º")),
    (7, ("7º")),
    (8, ("8º")),
    (9, ("9º")),
    (10, ("10º"))
)

class CustomAuthForm(AuthenticationForm):
    username = forms.CharField(widget=TextInput(attrs={'placeholder': 'Usuário'}))
    password = forms.CharField(widget=PasswordInput(attrs={'placeholder':'Senha'}))


class SignUpForm(UserCreationForm):
    birth_date = forms.DateField(widget=forms.TextInput(attrs={'placeholder': 'Data de Nascimento'}))
    password1 = forms.CharField(max_length=30, widget=forms.PasswordInput(attrs={'placeholder': 'Senha'}))
    password2 = forms.CharField(max_length=30, widget=forms.PasswordInput(attrs={'placeholder': 'Confirme Senha'}))

    class Meta:
        model = User
        widgets = {
            'username': forms.TextInput(attrs={'placeholder': 'Usuário'}),
            'email': forms.TextInput(attrs={'placeholder': 'E-mail'})
        }
        fields = ['username', 'email', 'birth_date', 'password1', 'password2']

    def get(self, data):
        return self.cleaned_data[data]

class EditProfileForm(UserChangeForm):
    birth_date = forms.DateField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Data de Nascimento'}))
    graduation_course = forms.CharField(required=False, max_length=30,widget=forms.TextInput(attrs={'placeholder': 'Curso de Graduação'}))
    graduation_period = forms.ChoiceField(required=False, choices=CHOICES, label="Período de Graduação")
    profile_image = forms.FileField(required=False)

    class Meta:
        model = User
        widgets = {
            'username': forms.TextInput(attrs={'placeholder': 'Usuário'}),
            'email': forms.TextInput(attrs={'placeholder': 'E-mail'}),
            'first_name': forms.TextInput(attrs={'placeholder': 'Primeiro Nome'}),
            'last_name': forms.TextInput(attrs={'placeholder': 'Último Nome'})
        }
        fields = ['username', 'email', 'first_name', 'last_name', 'birth_date','graduation_course', 'graduation_period','password', 'profile_image']

    def updateForm(self, user):
        if self.get('birth_date') != None:
            user.profile.birth_date = self.get('birth_date')

        if self.get('profile_image') != None:
            user.profile.profile_image = self.get('profile_image')

        if self.get('graduation_course') != None:
            user.profile.graduation_course = self.get('graduation_course')

        if self.get('graduation_period') != None:
            user.profile.graduation_period = self.get('graduation_period')

        return user


    def get(self, data):
        return self.cleaned_data[data]