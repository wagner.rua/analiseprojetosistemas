from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout


class Artist(models.Model):
    name = models.CharField(max_length=50, unique=True)
    biography = models.TextField(max_length=1000)
    genre = models.ManyToManyField("Genre")
    origin = models.CharField(max_length=30)
    rating = models.IntegerField(null=True, blank=True)
    profile_image = models.ImageField(default="Music-icon.png", blank=True)

    def __str__(self):
        return self.name


class Film(models.Model):
    name = models.CharField(max_length=50, unique=True)
    director = models.CharField(max_length=50)
    stars = models.ManyToManyField("FilmArtist")
    genre = models.ManyToManyField("FilmGenre")
    release_date = models.DateField(null=True, blank=True)
    rating = models.IntegerField(null=True, blank=True)
    profile_image = models.ImageField(default="Film-icon.png", blank=True)

    def __str__(self):
        return self.name

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    know = models.ManyToManyField("Profile", blank=True)
    birth_date = models.DateField(null=True, blank=True)
    graduation_course = models.CharField(max_length=30, blank=True, null=True)
    graduation_period = models.CharField(max_length=2,blank=True, null=True)
    profile_image = models.ImageField(default="profile.jpg", blank=True)
    blocked_people = models.ManyToManyField("Profile", related_name="block_people")
    likesArtist = models.ManyToManyField(Artist)
    likesFilm = models.ManyToManyField(Film)
    
    def __str__(self):
        return self.user.username

    @receiver(post_save, sender=User)
    def update_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)
        instance.profile.save()
    

    def likeArtist(self, artist):
        self.likesArtist.add(artist)

    def unlikeArtist(self, artist):
        self.likesArtist.remove(artist)

    def getLikeArtists(self):
        return self.likesArtist.all()

    def likeFilm(self, film):
        self.likesFilm.add(film)

    def unlikeFilm(self, film):
        self.likesFilm.remove(film)

    def getLikeFilms(self):
        return self.likesFilm.all()

    def addKnow(self, know):
        self.know.add(know)

    def removeKnow(self, know):
        self.know.remove(know)

    def getKnows(self):
        return self.know.all()

    def block(self, user):
        self.blocked_people.add(user)

    def removeBlock(self, user):
        self.blocked_people.remove(user)

    def getBlocks(self):
        return self.blocked_people.all()

    def login(self, request):
        auth_login(request, self.user)

    def logout(self, request):
        auth_logout(request)
        

class Genre(models.Model):
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name


class FilmGenre(models.Model):
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name

class FilmArtist(models.Model):
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name

class Search():

    def makeSearch(self, name_to_search):
        search = []

        search.append(Artist.objects.filter(name__icontains=name_to_search))
        search.append(User.objects.filter(username__icontains=name_to_search))
        search.append(Film.objects.filter(name__icontains=name_to_search))

        return enumerate(search)

    def getUser(self, username):
        return User.objects.get(username=username)

    def getArtist(self, name):
        return Artist.objects.get(name=name)

    def getFilm(self, name):
        return Film.objects.get(name=name)
